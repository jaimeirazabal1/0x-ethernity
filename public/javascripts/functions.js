function getTransactionId(){

  hash = document.getElementById('tx').value;
  $.ajax({

    url:serverApiUrl+"/confirmTransations",
    crossDomain : true,
    type:'post',
    data:{
      address:myAddress,
      hash:hash
    },
    success:function(r){
      console.log(r);
      if (r.error) {
        $('#txresult').html("<pre>"+JSON.stringify(r.error, undefined, 2)+"</<pre>")
      }else{
        $('#txresult').html("<pre>"+JSON.stringify(r.result, undefined, 2)+"</<pre>")
      }

    }
  })    
}


function createAccount(){

  var dk = keythereum.create();

  var options = {
    kdf: "pbkdf2",
    cipher: "aes-128-ctr",
    kdfparams: {
      c: 262144,
      dklen: 32,
      prf: "hmac-sha256"
    }
  };

  var password = "testpassword";
  var kdf = "pbkdf2";

  keythereum.dump(password, dk.privateKey, dk.salt, dk.iv, options, function (keyObject) {
    keythereum.recover(password, keyObject, function (privateKey) {
      $("#newAccount").html("<p><b>Address:</b></p>0x"+keyObject.address+"<br><p><b>Private Key:</b></p>0x"+privateKey.toString('hex'));
    });
  });


}

function sendOrder(){
  var order = {
  // The default web3 account address
  maker: $("#maker").val(),
  // Anyone may fill the order
  taker: $("#taker").val(),
  // The ZRX token contract on mainnet
  makerTokenAddress: $("#makerTokenAddress").val(),
  // The WETH token contract on mainnet
  takerTokenAddress: $("#takerTokenAddress").val(),
  // A BigNumber objecct of 1000 ZRX. The base unit of ZRX has 18
  // decimal places, the number here is 10^18 bigger than the
  // base unit.
  makerTokenAmount: web3.toWei($("#makerTokenAmount").val()),
  // A BigNumber objecct of 0.7 WETH. The base unit of WETH has
  // 18 decimal places, the number here is 10^18 bigger than the
  // base unit.
  takerTokenAmount: web3.toWei($("#takerTokenAmount").val()),
  // Add the duration (above) to the current time to get the unix
  // timestamp
  expirationUnixTimestampSec: parseInt(
    (new Date().getTime()/1000) + parseInt($("#duration").val()) * 60 * 60
    ).toString(),
  // We need a random salt to distinguish different orders made by
  // the same user for the same quantities of the same tokens.
  salt: ZeroEx.ZeroEx.generatePseudoRandomSalt()
  }

  // Get contract address

  // var zeroEx = new ZeroEx.ZeroEx(web3.currentProvider);
  // var addressPromise = zeroEx.exchange.getContractAddressAsync().then(
  //     (address) => {
  //         order.exchangeContractAddress = address;
  //    }
  //)
  order.exchangeContractAddress = "0x90fe2af704b34e0224bf2299c838e04d4dcf1364";


  // Contact relayer to retrieve fee

  var openrelayBaseURL = "https://api.openrelay.xyz";
  var feePromise = rp({
    method: 'POST',
    uri: openrelayBaseURL + "/v0/fees",
    body: order,
    json: true,
  }).then((feeResponse) => {
  // Convert the makerFee and takerFee into BigNumbers
  order.makerFee = new BigNumber(feeResponse.makerFee);
  order.takerFee = new BigNumber(feeResponse.takerFee);
  // The fee API tells us what taker to specify
  order.taker = feeResponse.takerToSpecify;
  // The fee API tells us what feeRecipient to specify
  order.feeRecipient = feeResponse.feeRecipient;
  })


  // Send the order
  Promise.all([order.exchangeContractAddress, feePromise]).then(() => {
  // Once those promises have resolved, our order is ready to be signed
  var orderHash = ZeroEx.ZeroEx.getOrderHashHex(order);
  return zeroEx.signOrderHashAsync(orderHash, order.maker);
  }).then((signature) => {
    order.ecSignature = signature;
    return order;
  });

}


function getBalance(){
  //var web3 = new Web3(new Web3.providers.HttpProvider(document.getElementById('rpc').value));
  web3.eth.getBalance(document.getElementById('myaddress').value,function(r,balance){
    document.getElementById('balance').innerHTML = "Your Balance is: "+ (balance/1e18) + " NoCash";
  });

}






function sendMoney(){
  //var web3 = new Web3(new Web3.providers.HttpProvider(document.getElementById('rpc').value));
  var privateKey = document.getElementById('pk').value;
  var toAddress = document.getElementById('to').value;
  var from = document.getElementById('from').value;
  var quantity = (parseFloat(document.getElementById('quantity').value)*1e18);

  web3.eth.getTransactionCount(from, function (err, nonce) {
    var rawTransaction = {"from":from, "gasPrice":0x2e90edd000,"gasLimit":0x33450,"to":toAddress,"value":quantity,"data":"","nonce":web3.toHex(nonce)}
    console.log('rawTransaction',rawTransaction)
    var tx = new ethereumjs.Tx(rawTransaction)
    tx.sign(ethereumjs.Buffer.Buffer.from(privateKey, 'hex'));

    var raw = '0x' + tx.serialize().toString('hex');
    web3.eth.sendRawTransaction(raw, function (err, transactionHash) {
      document.getElementById('txid').innerHTML = "Transaccion id: "+ transactionHash;
    });
  });   
}
