$(document).ready(function(){
    $("#maker").val(web3.eth.accounts[0]);

    $("#send_order_form").submit(function(e){
      e.preventDefault();
      var form = $(this);
      $.ajax({
        url:'/handleOrder',
        data:form.serialize(),
        type:'post',
        success:function(r){
          console.log(r);
        }
      })
    })
  });

