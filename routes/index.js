var express = require('express');
var ZeroEx = require('0x.js');
var web3 = require('web3');
var BigNumber = require('bignumber.js');
var router = express.Router();
var api_key = '8c41a1a79e3e4b6a841d28a415ad19aa'
var providerUrl = 'https://kovan.infura.io/'+api_key;
var providerUrl_v3 = 'https://kovan.infura.io/v3/8c41a1a79e3e4b6a841d28a415ad19aa';
var Web3 = require('web3');

if (typeof web3 !== 'undefined') {
  web3 = new Web3(web3.currentProvider);
} else {
  // set the provider you want from Web3.providers
  web3 = new Web3(new Web3.providers.HttpProvider(providerUrl_v3));
}
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: '0x and express' });
});
router.post('/handleOrder', function(req, res, next) {

	  var order = {
		  // The default web3 account address
		  maker: req.body.maker,
		  // Anyone may fill the order
		  taker: req.body.taker,
		  // The ZRX token contract on mainnet
		  makerTokenAddress: req.body.makerTokenAddress,
		  // The WETH token contract on mainnet
		  takerTokenAddress: req.body.takerTokenAddress,
		  // A BigNumber objecct of 1000 ZRX. The base unit of ZRX has 18
		  // decimal places, the number here is 10^18 bigger than the
		  // base unit.
		  makerTokenAmount: new BigNumber(req.body.makerTokenAmount*1e18),
		  // A BigNumber objecct of 0.7 WETH. The base unit of WETH has
		  // 18 decimal places, the number here is 10^18 bigger than the
		  // base unit.
		  takerTokenAmount: new BigNumber(req.body.takerTokenAmount*1e18),
		  // Add the duration (above) to the current time to get the unix
		  // timestamp
		  expirationUnixTimestampSec: parseInt(
		    (new Date().getTime()/1000) + parseInt(req.body.duration) * 60 * 60
		    ).toString(),
		  // We need a random salt to distinguish different orders made by
		  // the same user for the same quantities of the same tokens.
		  salt: ZeroEx.ZeroEx.generatePseudoRandomSalt()
	  }
	  	var zeroEx = new ZeroEx.ZeroEx(web3.currentProvider);
		var addressPromise = zeroEx.exchange.getContractAddressAsync().then(
		    (address) => {
		        order.exchangeContractAddress = address;
		    }
		)
  res.send({saludo:'hola',form:req.body,order:order});
});
module.exports = router;
